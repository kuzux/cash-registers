import type { Config } from 'tailwindcss'

const config: Config = {
    content: [
        './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
        './src/components/**/*.{js,ts,jsx,tsx,mdx}',
        './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    ],
    theme: {
        extend: {
            backgroundImage: {
                'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
                'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
            },
            animation: {
                'move-and-fade-out': 'moveAndFade 1s ease-in-out',
                'shake': 'shakeKeyframes 0.5s ease-in-out',
            },
            keyframes: theme => ({
                moveAndFade: {
                    '0%': { 
                        opacity: "100%"
                    },
                    '100%': {
                        opacity: '0%',
                        marginTop: '-2em'
                    },
                },
                shakeKeyframes: {
                    '0%': { marginLeft: '0' },
                    '5%': { marginLeft: '-0.5em' },
                    '15%': { marginLeft: '0.5em' },
                    '25%': { marginLeft: '-0.5em' },
                    '35%': { marginLeft: '0.5em' },
                    '45%': { marginLeft: '-0.5em' },
                    '55%': { marginLeft: '0.5em' },
                    '65%': { marginLeft: '-0.5em' },
                    '75%': { marginLeft: '0.5em' },
                    '85%': { marginLeft: '-0.5em' },
                    '95%': { marginLeft: '0.5em' },
                    '100%': { marginLeft: '0' },
                }
            }),
        },
    },
    plugins: [],
}
export default config
