"use client";

import { produce } from 'immer';
import { useEffect, useState } from 'react';

type FadingElementProps = {
    key?: any;
    children?: React.ReactNode;
    startFade: boolean;
    onAnimationEnd?: () => void;
}

function FadingElement(props: FadingElementProps) {
    let [fading, setFading] = useState(false);
    let [displaying, setDisplaying] = useState(true);

    if(!displaying) return null;

    let className = ""
    // defined in tailwind config
    if(fading) className = "animate-move-and-fade-out";

    if (props.startFade && !fading) setFading(true);

    return <div className={className} onAnimationEnd={() => {
        setDisplaying(false);
        if(props.onAnimationEnd) props.onAnimationEnd();
    }}>
        {props.children}
    </div>
}

type QueueItem = {
    uuid: string,
    count: number
}

function QueueDisplay(props: { items: QueueItem[] }) {
    let items = props.items.map((item, index) => {
        return <FadingElement startFade={item.count === 0} key={item.uuid}>
            <div className="bg-green-200 w-8 h-8 rounded-full text-black flex items-center justify-center">
                {item.count}
            </div>
        </FadingElement>
    });

    return <div className="flex flex-col gap-2">
        <div className="bg-red-500 w-8 h-8 mb-2 rounded-md"></div>
        { items }
    </div>
}

type IntegerInputProps = {
    onInput: (number: number) => void;
    placeholder?: string;
    positive?: boolean;
}

function IntegerInput(props: IntegerInputProps) {
    let [numberInput, setNumberInput] = useState("");
    let [shaking, setShaking] = useState(false);

    let handleAdd = () => {
        let number = parseInt(numberInput);
        let valid = !isNaN(number);
        if(props.positive === true) valid = valid && (number >= 0);
        if(!valid) {
            setShaking(true);
            setNumberInput("");
            return;
        }
        props.onInput(number);
    };

    let inputClasses = [
        "bg-gray-200",
        "rounded-l-md",
        "p-2",
        "text-black",
        "border-4", // always displaying a border avoids a small layout shift on error
    ];

    let buttonClasses = [
        "text-white",
        "font-bold",
        "py-2",
        "px-4",
        "rounded-r-md",
    ];

    let numRegex = /^\-?\d+$/;
    if(props.positive === true) numRegex = /^\d+$/;

    let buttonEnabled = numRegex.test(numberInput);
    let inputError = !buttonEnabled && (numberInput !== "");

    if(inputError)
        inputClasses.push("border-b-red-500");
    else
        inputClasses.push("border-gray-200");

    if(buttonEnabled) {
        buttonClasses.push("bg-blue-700");
        buttonClasses.push("hover:bg-blue-600");
    } else {
        buttonClasses.push("bg-gray-500");
    }

    let containerClasses = ["flex", "flex-row"];
    // defined in tailwind config
    if(shaking) containerClasses.push("animate-shake");

    let handleShakeEnd = () => {
        setShaking(shaking => {
            if(shaking) return false;
            return shaking;
        });
    };

    return <div className={containerClasses.join(" ")} onAnimationEnd={handleShakeEnd}>
        <input className={inputClasses.join(" ")} type="text" placeholder={props.placeholder} value={numberInput} onChange={(evt) => setNumberInput(evt.target.value)} />
        <button className={buttonClasses.join(" ")} onClick={handleAdd}>Add Item</button>
    </div>
}

export default function Home() {
    let [items, setItems] = useState<QueueItem[][]>([[], [], [], [], []]);

    let queueLength = (queue: QueueItem[]) => {
        return queue.reduce((acc, item) => acc + item.count, 0);
    };

    let addItem = (count: number) => {
        setItems(produce(draft => {
            let minIndex = -1;
            let minLength = Infinity;

            for(let i=0; i<draft.length; i++) {
                let queue = draft[i];
                let length = queueLength(queue);
                if(length < minLength) {
                    minLength = length;
                    minIndex = i;
                }
            }

            draft[minIndex].push({ count, uuid: crypto.randomUUID() });
        }));
    };

    let removeItemFromEach = () => {
        setItems(produce(draft => {
            for(let i=0; i<draft.length; i++) {
                let queue = draft[i];
                let nonzero = queue.filter(item => item.count > 0);
                // after an item is reduced to zero, we do not immediately remove it
                // since the animation triggers on count == 0
                // after the animation is complete, we can remove the item
                if(nonzero.length !== 0) nonzero[0].count--;
                draft[i] = nonzero;
            }
        }));
    };

    useEffect(() => {
        let interval = setInterval(removeItemFromEach, 1000);
        return () => clearInterval(interval);
    }, []);

    let displays = items.map((queue, index) => { 
        return <QueueDisplay key={index} items={queue} />
    });

    return <main className='flex flex-col gap-20 py-20'>
        <div className='flex flex-row gap-4 justify-center'>
            <IntegerInput onInput={addItem} placeholder='Number of items to add' positive />
        </div>
        <div className="flex flex-row gap-4 justify-center">
            { displays }
        </div>
    </main>
}
